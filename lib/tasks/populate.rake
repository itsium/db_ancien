# encoding: UTF-8
require 'nokogiri'
require 'open-uri'
require 'net/http'

namespace :db do

  desc "Fill database"
  task :populate => :environment do

    puts "----------------"
    puts "--- populate ---"
    puts "----------------"

    populate_studiants
  end
end

def populate_studiants   
  (13980..14060).each do |i|
     puts "---- Student #{i}  ----"
       _NOM = nil
       _PRENOM = nil
       _PROMO = nil
       _DIPLOME = []
       _EXP = []
       _IMG_URL = nil
       _IMG = nil

       doc = Nokogiri::HTML(open("http://anciens2.epitech.eu/aff_ancien/#{i}"))

       next if (doc.search(".titre_couleur").children.to_s == "Erreur")
       
       

       doc.search(".titre_couleur").each do |d|
         _NOM = d.next.next.text.strip! if d.child.text == "Nom"
         _PRENOM = d.next.next.text.strip! if d.child.text == "Prénom"
         _PROMO = d.next.next.text.strip! if d.child.text == "Promo"
         if d.child.text == "Diplôme(s)"
           d.parent.parent.xpath('tr').each do |t|
             _DIPLOME << t.children.text.strip! if t.children && t.children.text.strip! != "Diplôme(s)" && t.children.text.strip! != ""
           end
         end
       end

       _IMG = doc.search("#avatar").attribute('src').to_s
       if _IMG == "/img/avatars/vide.jpg"
         _IMG = nil 
       else
         _IMG_URL = "http://anciens2.epitech.eu#{_IMG}"
       end

       unless _IMG.nil?
         Net::HTTP.start("anciens2.epitech.eu") do |http|
             resp = http.get(_IMG)
             open("public/#{_IMG[1,_IMG.length]}", "wb") do |file|
                 file.write(resp.body)
             end
         end
       end

       doc.search(".groCouleur").last.parent.parent.next.search("tr").each do |ex|
         if ex.search('td').at(2).text.strip! != nil
           _EXP << {:post => ex.search('td').at(0).text.strip!,
                    :debut => ex.search('td').at(1).text.strip!,
                    :fin => ex.search('td').at(2).text.strip!,
                    :sociel => ex.search('td').at(3).text.strip!
                   }
         end
       end
       puts "NOM : #{_NOM} PRENOM : #{_PRENOM} PROMO : #{_PROMO}"
       puts "IMG : #{_IMG}"
       
       # t.string :poste
       #        t.string :debut
       #        t.string :fin
       #        t.string :societe
       #        t.integer :student_id
       
       
       # t.string :nom
       # t.string :prenom
       # t.string :promo
       # t.string :photo
       st = Student.create!(:nom => _NOM,
                            :prenom => _PRENOM,
                            :promo => _PROMO,
                            :photo => _IMG)
       
      _DIPLOME.each do |d|
        puts "DIPLOME : #{d}"
        st.diplomes.create!(:name => d)
      end
      
      _EXP.each do |e|
        puts "EXP : #{e[:post]} #{e[:sociel]} #{e[:debut]} - #{e[:fin]}"
        st.experiences.create!(:poste => e[:post],
                               :debut => e[:debut],
                               :fin => e[:fin],
                               :societe => e[:sociel])
      end
end

   





end