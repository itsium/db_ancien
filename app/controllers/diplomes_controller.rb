class DiplomesController < ApplicationController
  # GET /diplomes
  # GET /diplomes.json
  def index
    @diplomes = Diplome.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @diplomes }
    end
  end

  # GET /diplomes/1
  # GET /diplomes/1.json
  def show
    @diplome = Diplome.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @diplome }
    end
  end

  # GET /diplomes/new
  # GET /diplomes/new.json
  def new
    @diplome = Diplome.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @diplome }
    end
  end

  # GET /diplomes/1/edit
  def edit
    @diplome = Diplome.find(params[:id])
  end

  # POST /diplomes
  # POST /diplomes.json
  def create
    @diplome = Diplome.new(params[:diplome])

    respond_to do |format|
      if @diplome.save
        format.html { redirect_to @diplome, notice: 'Diplome was successfully created.' }
        format.json { render json: @diplome, status: :created, location: @diplome }
      else
        format.html { render action: "new" }
        format.json { render json: @diplome.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /diplomes/1
  # PUT /diplomes/1.json
  def update
    @diplome = Diplome.find(params[:id])

    respond_to do |format|
      if @diplome.update_attributes(params[:diplome])
        format.html { redirect_to @diplome, notice: 'Diplome was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @diplome.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /diplomes/1
  # DELETE /diplomes/1.json
  def destroy
    @diplome = Diplome.find(params[:id])
    @diplome.destroy

    respond_to do |format|
      format.html { redirect_to diplomes_url }
      format.json { head :no_content }
    end
  end
end
