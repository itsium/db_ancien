class Student < ActiveRecord::Base
  attr_accessible :nom, :photo, :prenom, :promo
  has_many :diplomes
  has_many :experiences
  
  def self.search(params)
    if params[:search]
      Student.find(:all, :conditions => ['nom LIKE ? or prenom LIKE ?', "%#{params[:search]}%" ,"%#{params[:search]}%"])
    else
      Student.order("nom").page(params[:page])
    end
  end
end
