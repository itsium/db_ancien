require 'test_helper'

class DiplomesControllerTest < ActionController::TestCase
  setup do
    @diplome = diplomes(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:diplomes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create diplome" do
    assert_difference('Diplome.count') do
      post :create, diplome: { name: @diplome.name, student_id: @diplome.student_id }
    end

    assert_redirected_to diplome_path(assigns(:diplome))
  end

  test "should show diplome" do
    get :show, id: @diplome
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @diplome
    assert_response :success
  end

  test "should update diplome" do
    put :update, id: @diplome, diplome: { name: @diplome.name, student_id: @diplome.student_id }
    assert_redirected_to diplome_path(assigns(:diplome))
  end

  test "should destroy diplome" do
    assert_difference('Diplome.count', -1) do
      delete :destroy, id: @diplome
    end

    assert_redirected_to diplomes_path
  end
end
