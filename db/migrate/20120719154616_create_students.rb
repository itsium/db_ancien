class CreateStudents < ActiveRecord::Migration
  def change
    create_table :students do |t|
      t.string :nom
      t.string :prenom
      t.string :promo
      t.string :photo

      t.timestamps
    end
  end
end
