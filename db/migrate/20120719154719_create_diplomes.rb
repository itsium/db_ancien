class CreateDiplomes < ActiveRecord::Migration
  def change
    create_table :diplomes do |t|
      t.string :name
      t.integer :student_id

      t.timestamps
    end
  end
end
