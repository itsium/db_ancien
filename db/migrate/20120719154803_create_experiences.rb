class CreateExperiences < ActiveRecord::Migration
  def change
    create_table :experiences do |t|
      t.string :poste
      t.string :debut
      t.string :fin
      t.string :societe
      t.integer :student_id

      t.timestamps
    end
  end
end
